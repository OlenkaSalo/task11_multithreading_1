package com.epum.Pipe;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedReader;

public class PipeIn extends Thread {
    PipedReader in ;




    public PipeIn(PipedReader in) {
        this.in = in;

    }

    @Override
    public void run()
    {
        try {
            int data = in.read();
            while(data!=-1)
            {
                data=in.read();
                System.out.print((char) data);


            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
