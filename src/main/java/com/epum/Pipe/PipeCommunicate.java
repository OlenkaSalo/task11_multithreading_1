package com.epum.Pipe;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedReader;
import java.io.PipedWriter;

public class PipeCommunicate {

    public static void pipeCommunicateThread(){
        PipedReader pipedReader = new PipedReader();
        PipedWriter pipedWriter = new PipedWriter();
        try {
            pipedWriter.connect(pipedReader);
            Thread t1 = new Thread(new PipeIn(pipedReader));
            Thread t2 =new Thread(new PipeOut(pipedWriter, "Helloo, I'm add some msg"));

            t1.start();
            t2.start();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        pipeCommunicateThread();
    }

}
