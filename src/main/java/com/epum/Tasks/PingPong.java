package com.epum.Tasks;

public class PingPong {
    public void play()
    {
        Thread player1 = new Thread(new Player1(this));
        Thread player2 = new Thread(new Player2(this));
        player1.start();
        player2.start();
    }

    public static void main(String[] args){
        PingPong pp = new PingPong();
        pp.play();

        }

    }


class Player1 extends Thread{

    PingPong pp;
    Player1(PingPong pP)
    {
      pp=pP;
    }

    @Override
    public void run()
    {
        synchronized (pp)
        {
            for(int i=0; i<1000;i++)
            {
                try {
                    System.out.println("Move the player 1"+" "+i++);
                    pp.notify();
                    pp.wait();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            pp.notify();
        }
    }


    }

    class Player2 extends Thread{
       PingPong pp;
          Player2(PingPong pP)
          {
              pp=pP;
          }

          @Override
       public void run() {
           synchronized (pp) {
               for (int i = 0; i < 1000; i++) {
                   try {
                       System.out.println("Move the player 2"+" "+i++);
                       pp.notify();
                       pp.wait();

                   } catch (InterruptedException e) {
                       e.printStackTrace();
                   }
               }
               pp.notify();
           }
       }


    }



